INTRODUCTION
------------

 This module redirects us to the search page when we press keywords on any page randomly except in the text areas or on drop downs.


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module.


CONFIGURATION
-------------

 *  There is no configuration. Whenenabled, the module will automatically attach to all our web pages and provide the search propery.

REQUIREMENTS
------------

 *The module use LAMP stack.
